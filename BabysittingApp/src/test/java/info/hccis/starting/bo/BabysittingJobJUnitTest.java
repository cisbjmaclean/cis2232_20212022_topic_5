package info.hccis.starting.bo;

import info.hccis.starting.exception.BabysittingJobInvalidException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author bjmac
 */
public class BabysittingJobJUnitTest {

    public BabysittingJobJUnitTest() {
    }

//    @org.junit.jupiter.api.BeforeAll
//    public static void setUpClass() throws Exception {
//        System.out.println("setUpClass running");
//    }
//
//    @org.junit.jupiter.api.AfterAll
//    public static void tearDownClass() throws Exception {
//        System.out.println("tearDownClass running");
//    }
//
//    @org.junit.jupiter.api.BeforeEach
//    public void setUp() throws Exception {
//        System.out.println("tearDown running");
//    }
//
//    @org.junit.jupiter.api.AfterEach
//    public void tearDown() throws Exception {
//        System.out.println("tearDown running");
//    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    // The methods must also start wtih the word test !!!!!!!!
    @Test
    public void test_calculateFee_0_hours() {

        BabysittingJob bsj = new BabysittingJob();
        bsj.setNumberOfHours(0);
        double feeActual = bsj.calculateFee();
        double feeExpected = 0;
        Assertions.assertEquals(feeExpected, feeActual);
    }

    @Test
    public void test_calculateFee_job0_emp1_children1_hours1() {

        BabysittingJob bsj;
        try {
            bsj = new BabysittingJob(0, 1, 1, 1);
            double feeActual = bsj.calculateFee();
            double feeExpected = 7;
            Assertions.assertEquals(feeExpected, feeActual);
        } catch (BabysittingJobInvalidException ex) {
            Logger.getLogger(BabysittingJobJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void test_calculateFee_job0_emp1_children2_hours1() {

        BabysittingJob bsj;
        try {
            bsj = new BabysittingJob(0, 1, 2, 1);
            double feeActual = bsj.calculateFee();
            double feeExpected = 14;
            Assertions.assertEquals(feeExpected, feeActual);
        } catch (BabysittingJobInvalidException ex) {

        }
    }

    @Test
    public void test_calculateFee_job0_emp1_children2_hours2() {

        BabysittingJob bsj;
        try {
            bsj = new BabysittingJob(0, 1, 2, 2);
            double feeActual = bsj.calculateFee();
            double feeExpected = 28;
            Assertions.assertEquals(feeExpected, feeActual);
        } catch (BabysittingJobInvalidException ex) {
            Logger.getLogger(BabysittingJobJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void test_calculateFee_job0_emp2_children1_hours1() {

        BabysittingJob bsj;
        try {
            bsj = new BabysittingJob(0, 2, 1, 1);
            double feeActual = bsj.calculateFee();
            double feeExpected = 9;
            Assertions.assertEquals(feeExpected, feeActual);
        } catch (BabysittingJobInvalidException ex) {
            Logger.getLogger(BabysittingJobJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void test_calculateFee_job0_emp2_children4_hours2() {

        BabysittingJob bsj;
        try {
            bsj = new BabysittingJob(0, 2, 4, 2);
            double feeActual = bsj.calculateFee();
            double feeExpected = 42;  //(9+(4*3))*2 hours)
            Assertions.assertEquals(feeExpected, feeActual);
        } catch (BabysittingJobInvalidException ex) {
            Logger.getLogger(BabysittingJobJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void test_calculateFee_job0_emp3_children2_hours2() {
        BabysittingJob bsj;
        try {
            bsj = new BabysittingJob(0, 3, 2, 2);
            double feeActual = bsj.calculateFee();
            double feeExpected = 26; //(9+4)*2
            Assertions.assertEquals(feeExpected, feeActual);
        } catch (BabysittingJobInvalidException ex) {
            Logger.getLogger(BabysittingJobJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void test_calculateFee_job0_emp1_children2_hours5() {

        BabysittingJob bsj;
        try {
            bsj = new BabysittingJob(0, 1, 2, 5);
            double feeActual = bsj.calculateFee();
            double feeExpected = 70;  //7*2*5
            Assertions.assertEquals(feeExpected, feeActual);
        } catch (BabysittingJobInvalidException ex) {
            Logger.getLogger(BabysittingJobJUnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Test the fact that the code will throw an exception if there is an invalid 
     * employee code.
     * @since 20211125
     * @author BJM
     */
    
    @Test
    public void test_constructor_job0_emp4_children1_hours1_invalidEmployee() {

        BabysittingJob bsj;
            double feeExpected = 1;  //Setting to 1
            double feeActual=-1;     //Setting not to 1;
        try {
            bsj = new BabysittingJob(0, 4, 1, 1);
        } catch (BabysittingJobInvalidException ex) {
            feeActual=1;   //If the exception is caught setting to 1;
        }
 
       Assertions.assertEquals(feeExpected, feeActual);
 
    }

     /**
     * Test the fact that the code will throw an exception if there is an invalid 
     * employee code.
     * @since 20211125
     * @author BJM
     */
    
    @Test
    public void test_setEmployee_emp4_invalidEmployee() {

        BabysittingJob bsj;
        try {
            bsj = new BabysittingJob();
            bsj.setEmployeeNumber(4);
            Assertions.fail("Excpetion for invalid employee type not thrown");
        } catch (BabysittingJobInvalidException ex) {
            //No need for anything.  

        }
 
    }

     /**
     * Test that employee name is set in constructor for employee number 1
     * @since 20211126
     * @author BJM
     */
    
    @Test
    public void test_setCustomConstructorSetsName_emp1() {

        BabysittingJob bsj;
        try {
            bsj = new BabysittingJob(0,1,1,1);
            String expected = "Cindy";
            Assertions.assertEquals(expected, bsj.getName());
        } catch (BabysittingJobInvalidException ex) {
            //No need for anything.  
        }
 
    }

}
