package info.hccis.starting.exception;

/**
 * 
 * @author bjmaclean
 * @since Nov 25, 2021
 */
public class BabysittingJobInvalidException extends Exception {

    public BabysittingJobInvalidException(String message){
        super(message);
    }
    
}
