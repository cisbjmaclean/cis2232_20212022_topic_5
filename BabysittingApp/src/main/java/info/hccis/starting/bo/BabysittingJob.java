package info.hccis.starting.bo;

//import org.junit.jupiter.api.Assertions;

import info.hccis.starting.exception.BabysittingJobInvalidException;


/**
 * A job for Georgette's babysitting service
 *
 * @author bjmac
 * @since 20211125
 */
public class BabysittingJob {

    public static final double HOURLY_RATE_EMP_1 = 7;
    public static final double HOURLY_RATE_EMP_2_3 = 9;
    public static final double HOURLY_RATE_EMP_2_3_ADDITIONAL = 4;

    private int jobNumber;
    private int employeeNumber;
    private int numberOfChildren;
    private int numberOfHours;

    //derived attributes
    private String name;
    private double fee;

    public BabysittingJob() {
        //default
//        Assertions.assertEquals(1, 2);

    }

    /**
     * Custom constructor 
     * @since 20211126
     * @author BJM
     */
    public BabysittingJob(int jobNumber, int employeeNumber, int numberOfChildren, int numberOfHours) throws BabysittingJobInvalidException {

        checkEmployeeNumber(employeeNumber);
        this.jobNumber = jobNumber;
        setEmployeeNumber(employeeNumber);
        this.numberOfChildren = numberOfChildren;
        this.numberOfHours = numberOfHours;
    }

    /**
     * Method to validate the employee number.  Exception will be thrown if invalid.
     * @since 20211125
     * @author BJM
     */
    public void checkEmployeeNumber(int employeeNumber) throws BabysittingJobInvalidException {
        if (employeeNumber < 1 || employeeNumber > 3) {
            throw new BabysittingJobInvalidException("Invalid employee number");
        }

    }

    /**
     * Calculate the fee of the job
     *
     * @since 20211122
     * @author BJM
     */
    public double calculateFee() {
        double fee = 0;
        if (employeeNumber == 1) {
            fee = HOURLY_RATE_EMP_1 * numberOfChildren * numberOfHours;
        } else {
            double additionChildrenRate = (numberOfChildren - 1) * HOURLY_RATE_EMP_2_3_ADDITIONAL;
            fee = (HOURLY_RATE_EMP_2_3 + additionChildrenRate) * numberOfHours;
        }
        return fee;
    }

    public int getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(int jobNumber) {
        this.jobNumber = jobNumber;
    }

    public int getEmployeeNumber() {
        return employeeNumber;
    }

    /**
     * Check for valid employee number, set it if valid, and also set the name.
     * @param employeeNumber
     * @throws BabysittingJobInvalidException
     * @since 20211126
     * @author BJM
     */
    public void setEmployeeNumber(int employeeNumber) throws BabysittingJobInvalidException {

        checkEmployeeNumber(employeeNumber);
        this.employeeNumber = employeeNumber;
        setName();

    }

    public String getName() {
        return name;
    }

    /**
     * Set the name based on the current employee number
     * @since 20211126
     * @author BJM
     */
    public void setName() {
        if(employeeNumber == 1){
            this.name = "Cindy";
        }
    }

    public int getNumberOfChildren() {
        return numberOfChildren;
    }

    public void setNumberOfChildren(int numberOfChildren) {
        this.numberOfChildren = numberOfChildren;
    }

    public int getNumberOfHours() {
        return numberOfHours;
    }

    public void setNumberOfHours(int numberOfHours) {
        this.numberOfHours = numberOfHours;
    }

    public double getFee() {
        return fee;
    }

}
