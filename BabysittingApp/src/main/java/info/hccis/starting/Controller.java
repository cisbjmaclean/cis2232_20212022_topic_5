package info.hccis.starting;

import info.hccis.util.CisUtility;
import java.util.Scanner;

/**
 *
 * @author bjmaclean
 * @since Oct 19, 2021
 */
public class Controller {

    public static final String MENU = "CIS Menu" + System.lineSeparator()
                + "A-do A" + System.lineSeparator()
                + "B-do B" + System.lineSeparator()
                + "C-do C" + System.lineSeparator()
                + "Q-Quit" + System.lineSeparator();

    
    public static void main(String[] args) {

        
        
        
        
        
        
        System.out.println("Welcome (" + CisUtility.getTodayString("yyyy-MM-dd") + ")");

        String option = "";

        while (!option.equalsIgnoreCase("Q")) {
            option = CisUtility.getInputString(MENU);
            option = option.toUpperCase();
            switch (option) {
                case "A":
                    processA();
                    break;
                case "B":
                    processB();
                    break;
                case "C":
                    processC();
                    break;
                case "Q":
                    System.out.println("thanks for using our program - have a good day");
                    break;
                default:
                    System.out.println("Invalid option");
            }
        }

    }

    public static void processA() {
        System.out.println("Hi... this is in the process A method");
    }

    public static void processB() {
        System.out.println("Hi... this is in the process B method");
    }

    public static void processC() {
        System.out.println("Hi... this is in the process C method");
    }

}
